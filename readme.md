# Optical Ray Tracing #

This project was developed as part of the UC Berkeley Physics 77 Capstone Project in 2016.
Authors: Matthew Shinner, Matthew Chow

# Features/Purpose #

With this program, you can model simple geometric optical systems featuring lenses and 1d objects, oriented on a optical axis.
By filling in the fields, object can be placed at a given height and x-axis position, and lenses can be created with a specified radius of 
curvature, index of refraction, height, and thickness.

To use, run display.py from the command line, and a tkinter GUI will appear. Click the add lens button to insert a lens, and click the add
object button to insert an object. Lenses and objects can be dragged with left mouse button, and deleted with the right mouse button or the clear
button. To edit an existing object, click on it and edit its parameters in the popup window. To see the ray path and projected image, click
the generate rays button. You can continue to drag optical elements to see the rays move in real time. 

This program uses the theory of matrix ray tracing (<https://en.wikipedia.org/wiki/Ray_transfer_matrix_analysis>) to determine the ray
paths. You can see the guiding matrix for a lens by clicking on it and lookit at its about-me popup window. 

# Dependencies #
* Python3
* NumPy
* Tkinter